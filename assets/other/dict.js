export default {
	novel: [
		'种田',
		'种田文',
		'种田流',
		'部落',
		'史前',
		'物种',
		'穿越',
		'穿越时空',
		'穿书',
		'快穿',
		'系统',
		'清穿',
		'武侠',
		'江湖',
		'侠客',
		'行侠仗义',
		'快意恩仇',
		'改文',
		'异能',
		'随身空间',
		'金手指',
		'玉佩',
		'打脸',
		'磨难',
		'星际',
		'日常',
		'爽文',
		'基建',
		'甜文',
		'甜宠',
		'萌宠',
		'主受',
		'主攻',
		'CP',
		'NPC',
		'搞笑',
		'完结',
		'番外',
		'轻松',
		'强推',
		'金推',
		'vip',
		'晋江',
		'起点',
		'书刊',
		'顶点',
		'笔趣阁',
		'黑岩',
		'PO18',
		'女强',
		'强强',
		'综漫',
		'综武侠',
		'综神话',
		'综影视',
		'HP',
		'火影',
		'斗罗',
		'无限',
		'无限流',
		'兑换',
		'主神',
		'耽美',
		'仙侠修真',
		'仙侠',
		'修真',
		'修仙',
		'神幻',
		'神怪',
		'洪荒',
		'宗门',
		'青龙',
		'朱雀',
		'玄武',
		'白虎',
		'神兽',
		'凤凰',
		'麒麟',
		'蛇妖',
		'蛇精',
		'九尾',
		'狐狸',
		'狐妖',
		'狐狸精',
		'狐精',
		'练功流',
		'热血',
		'丹药',
		'炼丹',
		'丹修',
		'药师',
		'剑修',
		'剑灵',
		'剑圣',
		'灵修',
		'妖修',
		'魔灵',
		'修士',
		'灵宠',
		'天仙',
		'仙女',
		'天神',
		'都市',
		'特种兵',
		'兵王',
		'男女',
		'玄幻',
		'魔法',
		'魔法师',
		'魔法少女',
		'马猴烧酒',
		'委员',
		'社团',
		'战斗',
		'养成',
		'废宅',
		'作家',
		'陷阱',
		'英雄',
		'Hero',
		'肥宅',
		'催眠',
		'野望',
		'小学生',
		'高中',
		'大学',
		'物语',
		'幻想',
		'幻想曲',
		'长篇',
		'短篇',
		'中篇',
		'狂想',
		'狂想曲',
		'东方',
		'大小姐',
		'女儿',
		'女友',
		'法师',
		'骑士',
		'战士',
		'西幻',
		'东幻',
		'大陆',
		'魔法幻情',
		'亡灵',
		'亡灵异族',
		'邪恶',
		'史诗',
		'史诗奇幻',
		'魔女',
		'女巫',
		'巫女',
		'巫师',
		'治愈',
		'治愈系',
		'怪物',
		'Monster',
		'Witch',
		'异世',
		'异界',
		'异世界',
		'勇者',
		'少女',
		'恶龙',
		'魔王',
		'轻小说',
		'冒险',
		'奇幻',
		'奇异',
		'召唤',
		'恶魔',
		'魅魔',
		'梦魇',
		'兽人',
		'尸鬼',
		'吸血鬼',
		'血族',
		'狼人',
		'魔兽',
		'转生',
		'硬核',
		'历史',
		'恐怖',
		'悬疑',
		'侦探',
		'警察',
		'女警',
		'案件',
		'间谍',
		'特工',
		'谍战',
		'改编',
		'推理',
		'盗墓',
		'倒斗',
		'阴谋',
		'摸金校尉',
		'变幻莫测',
		'奇事',
		'怪事',
		'诡异',
		'离奇',
		'灵异',
		'神怪',
		'网游',
		'全息',
		'科幻',
		'建设',
		'领袖',
		'领地',
		'领主',
		'同人',
		'剑三',
		'末日',
		'末世',
		'病毒',
		'丧尸',
		'生存',
		'求生',
		'荒岛',
		'小岛',
		'无人岛',
		'流落',
		'基地',
		'进化',
		'虫族',
		'异形',
		'灾变',
		'法则',
		'位面',
		'时代',
		'大佬',
		'言情',
		'古言',
		'古早',
		'虐文',
		'虐恋',
		'婚恋',
		'婚姻',
		'狗血',
		'现言',
		'现耽',
		'古耽',
		'豪门',
		'商业',
		'商战',
		'世家',
		'总裁',
		'黑化',
		'ooc',
		'都市情缘',
		'金丝雀',
		'娱乐圈',
		'带球跑',
		'怀孕',
		'初恋',
		'双洁',
		'双处',
		'追妻',
		'追妻火葬场',
		'海王',
		'渣男',
		'渣女',
		'影帝',
		'影后',
		'小白',
		'女尊',
		'师尊',
		'师父',
		'师傅',
		'师姐',
		'师兄',
		'师妹',
		'师弟',
		'神仙',
		'天师',
		'传承',
		'人鬼',
		'人鬼殊途',
		'人鬼情未了',
		'女鬼',
		'鬼怪',
		'幽灵',
		'茅山',
		'僵尸',
		'妖怪',
		'宠物',
		'成精',
		'修炼',
		'道士',
		'道长',
		'半仙',
		'风水',
		'水猴子',
		'水鬼',
		'鬼王',
		'鬼蜮',
		'地狱',
		'地府',
		'鬼差',
		'红衣',
		'恶鬼',
		'凶灵',
		'协议',
		'合约',
		'契约',
		'百鬼夜行',
		'布衣',
		'布衣生活',
		'田园',
		'农家',
		'农村',
		'山野人家',
		'山村',
		'重生',
		'电竞',
		'电子竞技',
		'竞技',
		'游戏',
		'死亡游戏',
		'综艺',
		'玛丽苏',
		'古风',
		'大明',
		'明朝',
		'大唐',
		'唐朝',
		'宋朝',
		'大宋',
		'春秋',
		'战国',
		'情有独钟',
		'破镜重圆',
		'阴差阳错',
		'天作之和',
		'天选之人',
		'天选之子',
		'平步青云',
		'天才',
		'太上皇',
		'皇帝',
		'女帝',
		'女王',
		'太女',
		'皇后',
		'太后',
		'太子',
		'奸臣',
		'公主',
		'驸马',
		'宫廷',
		'侯爵',
		'反派',
		'Boss',
		'千金',
		'小姐',
		'知己',
		'反派',
		'新婚',
		'妓女',
		'青楼',
		'怡红院',
		'女子',
		'娇妻',
		'妻子',
		'妻儿',
		'妻女',
		'妇女',
		'父女',
		'父子',
		'母子',
		'母女',
		'女儿',
		'孩子',
		'儿子',
		'收养',
		'孤儿',
		'妻离子散',
		'家破人亡',
		'惨绝人寰',
		'联姻',
		'校花',
		'死对头',
		'前任',
		'女配',
		'女主',
		'替身',
		'宠妻',
		'男主',
		'男配',
		'校草',
		'校霸',
		'逆袭',
		'屌丝',
		'炮灰',
		'霸总',
		'绿茶',
		'复仇',
		'深仇大恨',
		'血海深仇',
		'青梅',
		'竹马',
		'青梅竹马',
		'白月光',
		'姐妹',
		'姐弟',
		'兄妹',
		'骨科',
		'未来',
		'未来架空',
		'历史衍生',
		'1v1',
		'私设',
		'天降',
		'架空',
		'失忆',
		'现言',
		'男生',
		'女生',
		'女神',
		'民国',
		'租界',
		'出版',
		'纪实',
		'日记',
		'传记',
		'名著',
		'精校',
		'外传',
		'前传',
		'50年代',
		'五十年代',
		'五零',
		'60年代',
		'六十年代',
		'六零',
		'70年代',
		'七十年代',
		'七零',
		'80年代',
		'八十年代',
		'八零',
		'90年代',
		'九十年代',
		'九零',
		'年代文',
		'龙傲天',
		'升级流',
		'升级',
		'双胞胎',
		'双子',
		'灵魂',
		'灵魂转换',
		'Bl',
		'人兽',
		'处女',
		'处男',
		'触手',
		'换身',
		'姻缘',
		'百合',
		'女变男',
		'同性恋',
		'les',
		'拉拉',
		'lesbian',
		'gl',
		'扶她',
		'Futa',
		'双性',
		'变百',
		'性转',
		'变身',
		'生子',
		'男装',
		'女装',
		'abo',
		'Alpha',
		'Beta',
		'Omega',
		'高h',
		'Sm',
		'np',
		'后宫',
		'萝莉',
		'loli',
		'校园',
		'同桌',
		'师生',
		'病娇',
		'美食',
		'隐婚',
		'搞笑',
		'保镖',
		'无敌',
		'风俗',
		'二次元',
		'Acg',
		'史莱姆',
		'宅男',
		'宅女',
		'腐女',
		'系列',
		'作品',
		'娃娃'
	],
	comic: [
		'热血',
		'黑帮',
		'鬼才',
		'生活',
		'高中',
		'校队',
		'初中',
		'猎奇',
		'运动',
		'吸血鬼',
		'血族',
		'赛车',
		'勇者',
		'冒险',
		'冒险者',
		'小队',
		'公主',
		'国王',
		'恶龙',
		'史莱姆',
		'异世界',
		'异界',
		'异世',
		'穿越',
		'魔法',
		'魔物',
		'骷髅',
		'进化',
		'不死',
		'转生',
		'奇幻',
		'经典',
		'同伴',
		'学院',
		'魔术',
		'首席',
		'温馨',
		'喜剧',
		'魔族',
		'魔法少女',
		'马猴烧酒',
		'机战',
		'机甲',
		'装甲',
		'机器人',
		'战争',
		'人性',
		'人形',
		'人偶',
		'作品',
		'同人',
		'老物',
		'年代物',
		'单行本',
		'杂志',
		'周刊',
		'Acg',
		'四格',
		'搞笑',
		'日常',
		'欢乐',
		'女装',
		'感人',
		'沙雕',
		'女友',
		'男友',
		'前男友',
		'前女友',
		'前任',
		'大叔',
		'同事',
		'工作',
		'风俗',
		'灵异',
		'恐怖',
		'悬疑',
		'百合',
		'酒精',
		'yuri',
		'ゆり',
		'修仙',
		'修真',
		'末世',
		'末日',
		'丧尸',
		'病毒',
		'灾难',
		'宅男',
		'宅女',
		'腐女',
		'少女',
		'女生',
		'男生',
		'少年',
		'小孩',
		'后宫',
		'汉化组',
		'汉化',
		'中国语',
		'翻译',
		'国漫',
		'日漫',
		'风景',
		'事件',
		'美食',
		'真人',
		'青梅竹马',
		'恋爱',
		'工口',
		'エロ',
		'短篇',
		'长篇',
		'剧情',
		'骨科',
		'妹',
		'姉',
		'姐弟',
		'妹妹',
		'哥哥',
		'姐姐',
		'性转',
		'双性',
		'futa',
		'futanari',
		'扶她',
		'扶他',
		'萝莉',
		'幼女',
		'ロリ',
		'Loli',
		'Lolita',
		'Lolicon',
		'Cute',
		'可爱',
		'可愛い',
		'老师',
		'先生',
		'おば様',
		'おば',
		'人妻',
		'精灵',
		'妖精',
		'女仆',
		'护士',
		'女优',
		'偶像',
		'Idol',
		'忍者',
		'乡下',
		'乡村',
		'夏天',
		'夏日',
		'夏期',
		'暑假',
		'彩页',
		'全彩',
		'双胞胎',
		'双子',
		'荒岛',
		'小岛',
		'无人岛',
		'流落',
		'遭难',
		'游戏',
		'ゲーム',
		'社长',
		'诊所',
		'医院',
		'病気',
		'病院',
		'邻居',
		'青春期',
		'援交',
		'拷问',
		'韩漫',
		'奴隶',
		'可爱',
		'系列',
		'本子',
		'ntr',
		'Pixiv',
		'Cos',
		'Cosplay',
		'P站',
		'杂图',
		'插图',
		'散图',
		'学生',
		'幼稚',
		'小学生',
		'中学生',
		'初中生',
		'高中生',
		'大学生',
		'Js',
		'Jc',
		'Jk',
		'纯爱'
	],
	wallpaper: [
		'风景',
		'美女',
		'人物',
		'高清',
		'手机',
		'电脑',
		'动物',
		'4k',
		'3d',
		'AI',
		'A.I',
		'卡通',
		'动漫',
		'创意',
		'炫酷',
		'科技',
		'酷炫',
		'古风'
	],
	figure: [
		'Pixiv',
		'P站',
		'杂图',
		'插图',
		'散图'
	],
	portrait: [
		'明星',
		'模特',
		'Cos',
		'Cosplay',
		'二次元',
		'3d'
	],
	movie: [
		'恐怖',
		'喜剧',
		'戏剧',
		'戏曲',
		'搞笑',
		'灵异',
		'惊悚',
		'文艺',
		'科幻',
		'超级英雄',
		'漫威',
		'Dc',
		'爱情',
		'推理',
		'悬疑',
		'漫改',
		'奇幻',
		'古装',
		'僵尸',
		'经典',
		'港片',
		'香港',
		'大陆',
		'内地',
		'台湾',
		'日本',
		'美国',
		'欧美',
		'伦理'
	],
	drama: [
		'恐怖',
		'喜剧',
		'搞笑',
		'灵异',
		'惊悚',
		'文艺',
		'科幻',
		'超级英雄',
		'漫威',
		'Dc',
		'爱情',
		'推理',
		'悬疑',
		'漫改',
		'奇幻',
		'古装',
		'僵尸',
		'经典',
		'港片',
		'香港',
		'大陆',
		'内地',
		'台湾',
		'日本',
		'美国',
		'欧美',
		'伦理'
	],
	anime: [
		'日本',
		'国产',
		'大陆',
		'香港',
		'台湾',
		'布袋戏',
		'3d',
		'百合',
		'热血',
		'悬疑',
		'另类',
		'美国',
		'欧美'
	],
	variety: [
		'恐怖',
		'搞笑',
		'整蛊',
		'灵异',
		'惊悚',
		'明星',
		'爱情',
		'大陆',
		'内地',
		'香港',
		'台湾',
		'日本',
		'美国',
		'欧美',
		'伦理',
		'成人'
	],
	short: [
		'解说',
		'影视',
		'游戏',
		'日常',
		'Vlog',
		'记录',
		'探店',
		'生活',
		'歌舞'
	],
	mv: [
		'纯音乐',
		'古风',
		'流行',
		'Rap',
		'古筝',
		'钢琴',
		'虚拟歌手',
		'洛天依',
		'乐正绫',
		'初音未来',
		'刀郎'
	],
	live: [
		'游戏',
		'唱歌',
		'唱跳',
		'舞见',
		'舞蹈',
		'跳舞',
		'颜值',
		'情感',
		'聊天',
		'娱乐',
		'讲课',
		'线上课',
		'户外',
		'赛事',
		'体育',
		'电视'
	],
	broadcast: [
		'音乐',
		'唱歌',
		'情感',
		'聊天',
		'故事',
		'说书',
		'新闻'
	],
	music: [
		'纯音乐',
		'古风',
		'流行',
		'Rap',
		'古筝',
		'钢琴',
		'虚拟歌手',
		'洛天依',
		'乐正绫',
		'初音未来',
		'刀郎'
	],
	audible: [
		'小说',
		'戏曲',
		'评书',
		'相声',
		'广播剧',
		'百家讲坛'
	],
	article: [
		'人文',
		'生活',
		'灵异',
		'鬼怪',
		'科普',
		'奇闻',
		'轶事',
		'时事',
		'国际',
		'新闻',
		'散文'
	],
	news: [
		'人文',
		'生活',
		'灵异',
		'鬼怪',
		'科普',
		'奇闻',
		'轶事',
		'时事',
		'国际',
		'新闻',
		'散文'
	],
	forum: [
		'人文',
		'生活',
		'灵异',
		'鬼怪',
		'科普',
		'奇闻',
		'轶事',
		'时事',
		'国际',
		'新闻',
		'散文'
	]
}