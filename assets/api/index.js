import { sin_prepose } from '@/assets/api/global.js'

export default {
	//合集详情
	getGatherDetail (data) {
		return sin_prepose(data, 'getGatherDetail_' + data.type)
	},
	//合集作品列表
	getGatherWorksList (data) {
		return sin_prepose(data, 'getGatherWorksList_' + data.type)
	},
	//筛选详情
	getFilterDetail (data) {
		return sin_prepose(data, 'getFilterDetail_' + data.type)
	},
	//资源详情
	getSingleDetail (data) {
		return sin_prepose(data, 'getSingleDetail_' + data.type)
	},
	getSocketDetail (data) {
		return sin_prepose(data, 'getSocketDetail_' + data.type)
	},
	//分段详情
	getSectionDetail (data) {
		return sin_prepose(data, 'getSectionDetail_' + data.type)
	},
	//推荐
	getRecome (data) {
		return sin_prepose(data, 'getRecome_' + data.type)
	},
	//评论
	getComment (data) {
		return sin_prepose(data, 'getComment_' + data.type)
	},
	//获取用户详情
	getCharacterDetail(data) {
		return sin_prepose(data, 'getCharacterDetail_' + data.type)
	},
	//用户作品列表
	getCharacterWorksList (data, type) {
		return sin_prepose(data, 'getCharacterWorksList_' + data.type + '_' + type)
	},
	//用户关注列表
	getCharacterFolloweeList (data) {
		return sin_prepose(data, 'getCharacterFolloweeList_' + data.type)
	},
	//用户粉丝列表
	getCharacterFollowerList (data) {
		return sin_prepose(data, 'getCharacterFollowerList_' + data.type)
	},
	//处理socket消息
	handleSocketMessage (data) {
		return sin_prepose(data, 'handleSocketMessage')
	}
}