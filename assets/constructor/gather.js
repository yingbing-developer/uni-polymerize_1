//集合
import Utils from '@/uni_modules/yingbing-ui/js_sdk/util.js'

const { dateFormat } = Utils;

export default class Gather {
	constructor({id, gatherId, commentId, title, cover, desc, list, characters, comments, creator, avatar, isPay, readnum, popular, collect, thumbs, latest, comment, style, tag, isAdult, record, type, createTime, updateTime, extra, source}) {
		this.id = id || ((gatherId || '') + '_' + source)//资源唯一ID 必填 如果填了gatherId 可以不填
		this.gatherId = gatherId || '' //集合ID
		this.commentId = commentId || '' //评论ID
		this.title = title || ''//名称
		this.cover = cover || '' //封面
		this.desc = desc || '' //介绍
		this.list = list || [] //集合列表
		this.characters = characters || []//相关用户列表
		this.comments = comments || []//评论列表
		this.creator = creator || '' //创建者
		this.avatar = avatar || ''//创建者头像
		this.isPay = isPay || false//是否支付观看
		this.readnum = readnum || 0 //查看次数
		this.popular = popular || 0//热度
		this.collect = collect || 0//收藏数
		this.comment = comment || 0//评论数
		this.thumbs = thumbs || 0//点赞数
		this.latest = latest || ''//最后更新
		this.style = style || ''//风格
		this.tag = tag || []//标签
		this.isAdult = isAdult || false//是否青壮年内容
		this.record = record || {} //历史记录
		this.extra = extra || {} //附加字段
		this.type = type//类型
		this.createTime = createTime || dateFormat(new Date().getTime())//创建时间
		this.updateTime = updateTime || ''//更新时间
		this.source = source || 'local' //来源
	}
}