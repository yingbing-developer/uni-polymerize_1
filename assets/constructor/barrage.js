//弹幕
import Util from '@/uni_modules/yingbing-ui/js_sdk/util.js'

export default class Barrage {
	constructor({id, barrageId, avatar, name, text, color, time, type, extra, source}) {
		this.id = id || ((barrageId || '') + '_' + source)
		this.barrageId = barrageId || ''
		this.avatar = avatar || ''//发送人头像
		this.name = name || ''//发送人名称
		this.text = text || ''//内容
		this.color = color ? color.indexOf('rgb') > -1 ? Util.rgb2hex(color) : color.indexOf('rgba') > -1 ? Util.rgba2hex(color) : color : '#ffffff'//弹幕内容颜色
		this.time = time || 0//弹幕显示时间（单位是s）
		this.type = type || 'user'//类型
		this.extra = extra || {}//附加字段
		this.source = source || 'local'//来源
	}
}