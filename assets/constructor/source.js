//來源

export default class Source {
	constructor({id, title, logo, href, interval, security, cover, isAdult, custom, request, text}) {
	  this.id = id
	  this.title = title
	  this.logo = logo || ''
	  this.href = href
	  this.interval = interval || 0
	  this.security = security || ''
	  this.cover = cover || ''
	  this.isAdult = isAdult || false
	  this.custom = custom || {}
	  this.request = request || {}
	  this.text = text
	}
}