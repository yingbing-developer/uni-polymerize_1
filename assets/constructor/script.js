//浏览器脚本
import Utils from '@/uni_modules/yingbing-ui/js_sdk/util.js'

const { dateFormat } = Utils;

export default class Script {
	constructor({id, content, isOpen, type, isAdult, createTime}) {
		this.id = id || new Date().getTime().toString() + Math.round(Math.random() * 10000) + '_' + this.type
		this.content = content
		this.isOpen = isOpen
		this.type = type
		this.isAdult = isAdult || false
		this.createTime = createTime || dateFormat(new Date().getTime())
	}
}