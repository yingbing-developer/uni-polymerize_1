const commentMixin = {
	data () {
		return {
			currentCommentId: '',
			comments: [],
			commentLoading: false,
			commentCurrentPage: 1,
			commentPaging: {}
		}
	},
	methods: {
		async getComment () {
			return await this.app.$api.getComment(Object.assign({}, this.params, {
				currentPage: this.commentCurrentPage,
				paging: this.commentPaging,
				commentId: this.currentCommentId || this.params.commentId
			})).then((res) => {
				return res
			})
		},
		onCommentPulldown (callback) {
			this.commentCurrentPage = 1
			this.commentPaging = {}
			this.getComment().then(res => {
				this.commentLoading = false
				if ( res.code == this.app.$config.ERR_OK ) {
					callback && callback('success')
					this.commentPaging = res.data.paging || {}
					this.comments = res.data.list
					let el = this.app.$utils.typeof(this.$refs.comment) == 'Array' ? this.$refs.comment[0] : this.$refs.comment
					res.data.isLastPage ? el.setLoadmoreEnd() : null
				} else {
					callback && callback('fail')
					this.comments = this.params?.comments || []
					this.$refs.comment.setLoadmoreEnd()
				}
			})
		},
		onCommentLoadmore (callback) {
			this.commentCurrentPage++
			this.getComment().then(res => {
				if ( res.code == this.app.$config.ERR_OK ) {
					callback(res.data.isLastPage ? 'end' :  'success')
					this.commentPaging = res.data.paging || {}
					this.comments = this.comments.concat(res.data.list)
				} else {
					callback('fail')
				}
			})
		}
	},
	watch: {
		currentCommentId (newVal) {
			if ( newVal ) {
				this.comments = []
			}
		}
	}
}

export default commentMixin