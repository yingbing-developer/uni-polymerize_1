const recomeMixin = {
	data () {
		return {
			recomeParams: null,
			recomes: [],
			recomeLoading: true,
			recomeCurrentPage: 1,
			recomeRequestTime: 0,
			recomeIsLastPage: false,
			recomeRequestIndex: 0,
			recomePagings: {},
			recomeIsLastPages: {},
			recomeBy: 'record'
		}
	},
	methods: {
		async getRecome () {
			return await this.app.$business.getRecome({
				params: this.recomeParams || this.params,
				type: this.recomeParams ? this.recomeParams.type : this.params.type,
				pagings: this.recomePagings,
				requestTime: this.recomeRequestTime,
				currentPage: this.recomeCurrentPage,
				isLastPages: this.recomeIsLastPages,
				requestIndex: this.recomeRequestIndex,
				by: this.recomeBy
			}).then(data => {
				return data
			})
		},
		onRecomePulldown (callback) {
			this.recomeCurrentPage = 1
			this.recomeRequestTime = 0
			this.recomeRequestIndex = 0
			this.recomeIsLastPages = {}
			this.recomePagings = {}
			this.getRecome().then(data => {
				callback && callback(data.list.length > 0 ? 'success' : 'fail')
				this.recomeLoading = false
				this.recomes = data.list
				this.recomeIsLastPage = data.isLastPage
				let el = this.app.$utils.typeof(this.$refs.recome) == 'Array' ? this.$refs.recome[0] : this.$refs.recome
				data.isLastPage ? el.setLoadmoreEnd() : data.list.length == 0 ? el.setLoadmoreFail() : null
				this.recomeRequestTime = data.requestTime
				this.recomeCurrentPage = data.currentPage
				this.recomeIsLastPages = data.isLastPages
				this.recomeRequestIndex = data.requestIndex
				this.recomePagings = data.pagings
			})
		},
		onRecomeLoadmore (callback) {
			this.getRecome().then(data => {
				callback && callback(data.isLastPage ? 'end' : data.list.length > 0 ? 'success' : 'fail')
				this.recomes = this.recomes.concat(data.list)
				this.recomeIsLastPage = data.isLastPage
				this.recomeRequestTime = data.requestTime
				this.recomeCurrentPage = data.currentPage
				this.recomeIsLastPages = data.isLastPages
				this.recomePagings = data.pagings
				this.recomeRequestIndex = data.requestIndex
			})
		}
	}
}

export default recomeMixin