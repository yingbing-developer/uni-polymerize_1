<template>
	<view class="yb-flex">
		<yb-popup popout="fade" direction="bottom" :visible="show" @hide="hide">
			<view class="yb-flex" :style="{'background-color': skinColor.color_bg_2, height: height + 'px'}">
				<view class="title padding-gap yb-flex yb-row yb-align-center yb-justify-between">
					<yb-text :size="28" :color="skinColor.color_1" value="选择收藏夹" />
					<view class="yb-flex yb-row yb-align-center" @tap="createCollectFolder(null)">
						<yb-icon name="inc" :size="24" :color="skinColor.color_3"></yb-icon>
						<yb-text :size="24" :color="skinColor.color_3" value="新建收藏夹" />
					</view>
				</view>
				<yb-list
				custom-class="yb-flex-1"
				:loadmore="!isLastPage"
				@loadmore="onLoadmore">
					<yb-list-item>
						<view class="file-list yb-row yb-align-center" :style="{'border-bottom-color': skinColor.color_gap_1}" @tap="setCollectBy('default')">
							<view class="file-list-icon yb-flex yb-align-center yb-justify-center">
								<yb-icon name="folder-open-fill" size="75" color="#FFCA28" ></yb-icon>
							</view>
							<view class="file-list-right yb-flex yb-flex-1 yb-justify-between">
								<view class="yb-flex">
									<text class="file-list-title" :style="{'color': skinColor.color_1}">默认收藏夹</text>
									<text class="file-list-text" :style="{'color': skinColor.color_3}">{{defaultList.length + '个内容'}}</text>
								</view>
							</view>
							<yb-icon :name="collectBys.indexOf('default') > -1 ? 'check-rect-fill' : 'check-rect-no'" size="45" :color="collectBys.indexOf('default') > -1 ? skinColor.color_theme_1 : skinColor.color_3"></yb-icon>
						</view>
					</yb-list-item>
					<yb-list-item v-for="(folder, i) in collectfolderList.slice(0, currentPage * size)" :key="i">
						<view class="file-list yb-row yb-align-center" :style="{'border-bottom-color': skinColor.color_gap_1}" @tap="setCollectBy(folder.id)" @longpress="onFolderLongpress(folder)">
							<view class="file-list-icon yb-flex yb-align-center yb-justify-center">
								<yb-icon name="folder-open-fill" size="75" color="#FFCA28" ></yb-icon>
							</view>
							<view class="file-list-right yb-flex yb-flex-1 yb-justify-between">
								<view class="yb-flex">
									<text class="file-list-title" :style="{'color': skinColor.color_1}">{{folder.title}}</text>
									<text class="file-list-text" :style="{'color': skinColor.color_3}">{{folder.createTime + ' - ' + folder.list.length + '个内容'}}</text>
								</view>
							</view>
							<yb-icon :name="collectBys.indexOf(folder.id) > -1 ? 'check-rect-fill' : 'check-rect-no'" size="45" :color="collectBys.indexOf(folder.id) > -1 ? skinColor.color_theme_1 : skinColor.color_3"></yb-icon>
						</view>
					</yb-list-item>
				</yb-list>
				<yb-button :options="{
					bgColor: skinColor.color_3
				}" plain custom-class="copy-btn" @click="confirm" value="完成"></yb-button>
			</view>
		</yb-popup>
		<yb-dialog ref="dialog"></yb-dialog>
		<yb-notify ref="notify"></yb-notify>
		<yb-action-sheet ref="actionSheet"></yb-action-sheet>
	</view>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	export default {
		mixins: [appMixin],
		computed: {
			height () {
				return this.windowHeight / 1.5
			},
			data () {
				return this.$store.getters['collection/get'].filter(item => (!item.isAdult || this.$store.getters['app/getAdult']) && !this.app.$config.characterTypes.single[item.type])
			},
			defaultList () {
				return this.data.filter(item => !item.collectBy && item.type != 'collectfolder')
			},
			collectfolderList () {
				return this.data.filter(item => item.type == 'collectfolder')
			},
			isLastPage () {
				return this.currentPage * this.size >= this.collectfolderList.length
			}
		},
		data () {
			return {
				show: false,
				params: null,
				list: [],
				currentPage: 1,
				size: 20,
				windowHeight: 0,
				collectBys: [],
				removeCollectBys: []
			}
		},
		onLoad() {
			this.show = true
			this.windowHeight = uni.getSystemInfoSync().windowHeight
			this.params = this.app.$Route.query.params ? JSON.parse(decodeURIComponent(this.app.$Route.query.params)) : null
			this.list = this.app.$Route.query.list ? JSON.parse(decodeURIComponent(this.app.$Route.query.list)) : []
			if ( this.params ) {
				if ( this.defaultList.findIndex(item => item.id == this.params.id) > -1 ) {
					this.collectBys.push('default')
				}
				this.collectfolderList.forEach(folder => {
					if ( folder.list.findIndex(item => item.id == this.params.id) > -1 ) {
						this.collectBys.push(folder.id)
					}
				})
			}
			if ( this.collectBys.length == 0 ) {
				this.collectBys.push('default')
			}
		},
		methods: {
			setCollectBy (id) {
				let index = this.collectBys.indexOf(id)
				let removeIndex = this.removeCollectBys.indexOf(id)
				if ( index > -1 ) {
					this.collectBys.splice(index, 1)
					if ( removeIndex == -1 ) {
						this.removeCollectBys.push(id)
					}
				} else {
					this.collectBys.push(id)
					if ( removeIndex > -1 ) {
						this.removeCollectBys.splice(removeIndex, 1)
					}
				}
			},
			confirm () {
				let arr = []
				if ( this.list.length > 0 ) {
					arr = this.list
				} else {
					arr.push(this.params)
				}
				arr.forEach(item => {
					this.collectBys.forEach(collectBy => {
						if ( collectBy == 'default' ) {
							if ( this.defaultList.findIndex(collection => collection.id == item.id) == -1 ) {
								this.$store.dispatch('collection/add', item)
							}
						} else {
							let index = this.collectfolderList.findIndex(folder => folder.id == collectBy)
							let folder = this.collectfolderList[index]
							if ( folder.list.findIndex(folderItem => folderItem.id == item.id) == -1 ) {
								folder.list.unshift(item)
							}
							this.$store.dispatch('collection/add', folder)
						}
					})
					this.removeCollectBys.forEach(removeCollectBy => {
						if ( removeCollectBy == 'default' ) {
							if ( this.defaultList.findIndex(collection => collection.id == item.id) > -1 ) {
								this.$store.dispatch('collection/remove', item.id)
							}
						} else {
							let index = this.collectfolderList.findIndex(folder => folder.id == removeCollectBy)
							let folder = this.collectfolderList[index]
							let itemIndex = folder.list.findIndex(folderItem => folderItem.id == item.id)
							if ( itemIndex > -1 ) {
								folder.list.splice(itemIndex, 1)
							}
							this.$store.dispatch('collection/add', folder)
						}
					})
				})
				uni.showToast({
					title: '操作完成',
					icon: 'none'
				})
				this.show = false
			},
			hide () {
				getApp().globalData.$Router.back()
			},
			onFolderLongpress (params) {
				this.$refs.actionSheet.show({
					title: {
						text: params.title,
						color: this.skinColor.color_2
					},
					itemList: [{
						label: '编辑收藏夹',
						value: 'edit'
					},{
						label: '删除收藏夹',
						value: 'remove'
					},{
						label: '清空收藏内容',
						value: 'clear'
					}],
					options: {
						cancel: {
							text: '关闭',
							color: this.skinColor.color_1
						},
						bgColor: this.skinColor.color_bg_2,
						color: this.skinColor.color_1,
						border: {
							color: this.skinColor.color_gap_1,
						}
					},
					success: (res) => {
						if ( res.confirm ) {
							if ( res.data.detail.value == 'edit' ) {
								this.createCollectFolder(params)
							}
							if ( res.data.detail.value == 'remove' ) {
								this.$store.dispatch('collection/remove', params.id)
								this.$refs.notify.success('删除成功')
							}
							if ( res.data.detail.value == 'clear' ) {
								let checkes = Object.assign({}, this.app.$config.singleTypes, this.app.$config.gatherTypes)
								this.$refs.dialog.prompt({
									title: {
										color: this.skinColor.color_1,
										text: '清空收藏'
									},
									form: [{
										type: 'checkbox',
										required: true,
										row: true,
										wrap: true,
										checkboxs: Object.keys(checkes).map(key => {
											return {
												label: {
													text: checkes[key],
													color: this.skinColor.color_3
												},
												value: key,
												options: {
													shape: 'square',
													color: this.skinColor.color_3,
													bgColor: this.skinColor.color_bg_1,
													focus: this.skinColor.color_actived_1
												}
											}
										})
									}],
									options: {
										bgColor: this.skinColor.color_bg_2
									},
									success: res => {
										if ( res.confirm ) {
											if ( res.form[0].value && res.form[0].value.length > 0 ) {
												res.form[0].value.forEach(type => {
													params.list = params.list.filter(item => item.type != type)
												})
												this.$store.dispatch('collection/add', params)
												this.$refs.notify.success('成功清空' + params.title + ' 收藏夹下的' + res.form[0].value.map(key => this.app.$business.getType(key)).toString())
											}
										}
									}
								})
							}
						}
					}
				})
			},
			createCollectFolder (params) {
				let form = [{
					placeholder: '请输入收藏夹名称',
					required: true,
					value: params ? params.title : '',
					options: {
						color: this.skinColor.color_1
					}
				}]
				if ( this.$store.getters['app/getAdult'] ) {
					form.push({
						type: 'checkbox',
						value: params && params.isAdult.toString() == 'true' ? [params.isAdult.toString()] : [],
						checkboxs: [{
							label: {
								text: '是否敏感收藏',
								color: this.skinColor.color_3
							},
							value: 'true',
							options: {
								shape: 'square',
								color: this.skinColor.color_3,
								bgColor: this.skinColor.color_bg_1,
								focus: this.skinColor.color_actived_1
							}
						}]
					})
				}
				this.$refs.dialog.prompt({
					title: {
						color: this.skinColor.color_1,
						text: params ? '编辑收藏夹' : '新建收藏夹'
					},
					form: form,
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							if ( params ) {
								params.title = res.form[0].value
								params.isAdult = this.$store.getters['app/getAdult'] ? res.form[1].value && res.form[1].value[0] ? true : false : false
								this.$store.dispatch('collection/add', params)
								this.$refs.notify.success('编辑收藏夹成功')
							} else {
								let id = new Date().getTime().toString() + Math.floor(Math.random() * Math.pow(10, 5)) + '_collectfolder'
								let index = this.$store.getters['collection/get'].findIndex(item => item.id == id)
								if ( index > -1 ) {
									this.$refs.notify.warning('随机ID重复，请重新创建')
									return
								}
								this.$store.dispatch('collection/add', {
									id: id,
									title: res.form[0].value,
									list: [],
									createTime: this.app.$utils.dateFormat(new Date().getTime()),
									isAdult: this.$store.getters['app/getAdult'] ? res.form[1].value && res.form[1].value[0] ? true : false : false,
									type: 'collectfolder'
								})
								this.$refs.notify.success('创建收藏夹成功')
							}
						}
					}
				})
			}
		}
	}
</script>

<style>
	.title {
		padding: 20rpx 0;
	}
	.file-list {
		border-bottom-width: 1px;
		border-bottom-style: solid;
		padding: 30rpx 50rpx;
	}
	.file-list-icon {
		margin-right: 30rpx;
		width: 75rpx;
	}
	.file-list-icon {
		width: 75rpx;
		height: 75rpx;
	}
	.file-list-right {
		margin-right: 20rpx;
	}
	.file-list-title {
		flex: 1;
		font-size: 30rpx;
		margin-bottom: 10rpx;
		/* #ifdef APP-NVUE */
		lines: 1;
		text-overflow: ellipsis;
		/* #endif */
		/* #ifndef APP-NVUE */
		display: -webkit-box !important;
		overflow: hidden;
		text-overflow: ellipsis;
		word-break: break-all;
		-webkit-box-orient: vertical;
		-webkit-line-clamp: 1;
		/* #endif */
	}
	.file-list-text {
		font-size: 24rpx;
	}
	.copy-btn {
		height: 90rpx;
	}
</style>
