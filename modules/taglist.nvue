<template>
	<yb-popup popout="fade" direction="bottom" :visible="show" @hide="hide">
		<view class="yb-flex" :style="{'background-color': skinColor.color_bg_2, height: height + 'px'}">
			<yb-text class="title" :size="24" align="center" :color="skinColor.color_3" :value="'#' + keyword" />
			<rd-cells
			:list="list"
			ref="list"
			:empty="list.length == 0 && isLastPage"
			:loading="loading"
			loadmore
			@loadmore="onLoadmore">
			</rd-cells>
		</view>
	</yb-popup>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	export default {
		mixins: [appMixin],
		computed: {
			height () {
				return this.windowHeight / 1.5
			}
		},
		data () {
			return {
				show: false,
				list: [],
				type: '',
				keyword: '',
				loading: true,
				isLastPage: false,
				pagings: {},
				isLastPages: {},
				requestTime: 0,
				currentPage: 1,
				windowHeight: 0
			}
		},
		onLoad() {
			this.show = true
			this.keyword = decodeURIComponent(getApp().globalData.$Route.query.keyword)
			this.type = getApp().globalData.$Route.query.type
			this.windowHeight = uni.getSystemInfoSync().windowHeight
		},
		onReady () {
			this.$nextTick(function () {
				setTimeout(() => {
					this.getList().then(data => {
						this.loading = false
						this.list = data.list
						data.isLastPage ? this.$refs.list.setLoadmoreEnd() : data.list.length == 0 ? this.$refs.list.setLoadmoreFail() : null
						this.isLastPage = data.isLastPage
						this.pagings = data.pagings
						this.isLastPages = data.isLastPages
						this.currentPage = data.currentPage
						this.requestTime = data.requestTime
						this.loading = false
					})
				}, 500)
			})
		},
		methods: {
			async getList () {
				return await this.app.$business.requestApis({
					name: 'search',
					type: this.type,
					data: {
						keyword: this.keyword,
						sort: this.sort,
						currentPage: this.currentPage
					},
					pagings: this.pagings,
					isLastPages: this.isLastPages,
					requestTime: this.requestTime
				}).then(data => {
					return data
				})
			},
			onLoadmore (callback) {
				this.getList().then(data => {
					callback(data.isLastPage ? 'end' : data.list.length > 0 ? 'success' : 'fail')
					this.list = this.list.concat(data.list)
					this.isLastPage = data.isLastPage
					this.pagings = data.pagings
					this.isLastPages = data.isLastPages
					this.currentPage = data.currentPage
					this.requestTime = data.requestTime
				})
			},
			hide () {
				getApp().globalData.$Router.back()
			}
		}
	}
</script>

<style scoped>
.title {
	padding: 20rpx 0;
}
</style>
