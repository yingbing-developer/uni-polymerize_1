import Util from '../../../js_sdk/util.js'
export default {
	computed: {
		current () {
			return this.pages.findIndex(item => item.dataId == this.currentDataId)
		},
		prevDataId () {
			return this.pages[this.current - 1] && this.pages[this.current - 1].dataId
		},
		nextDataId () {
			return this.pages[this.current + 1] && this.pages[this.current + 1].dataId
		}
	},
	data() {
		return {
			currentDataId: -1,
			isShow: false,
			viewWidth: 0,
			moreLoading: false
		}
	},
	methods: {
		//翻往上一页
		pagePrevFlip () {
			this.$refs.flip.flipToPrev()
		},
		//翻往下一页
		pageNextFlip () {
			this.$refs.flip.flipToNext()
		},
		reloadLoadmoreFlip (p) {
			let loadIndex = this.pages.findIndex(page => p.dataId == page.dataId)
			this.$set(this.pages[loadIndex], 'type', 'loading')
			let nextChapter = p.direction == 'next' ? p.chapter + 1 : p.chapter - 1
			this.loadmoreFlip(nextChapter, p.direction == 'next' ? 1 : -1);
		},
		loadmoreFlip (chapter, value) {
			this.$emit('loadmore', chapter, (status, content) => {
				this.moreLoading = false;
				if (status == 'success') {
					const index = this.contents.findIndex(item => item.chapter == content.chapter)
					if (index > -1) {
						this.contents[index] = content;
					} else {
						this.contents.push(content);
					}
					this.computedPage({
						content: content,
						type: value > 0 ? 'next' : 'prev'
					});
					this.preload(chapter)
				} else {
					let loadIndex = this.pages.findIndex(page => page.type == 'loading' && page.direction == (value > 0 ? 'next' : 'prev'))
					this.$set(this.pages[loadIndex], 'type', status)
				}
			})
		},
		handleFlipChangeRender (e) {
			this.handleFlipChange(e.detail.dataId)
		},
		handleFlipChange (dataId) {
			const value = dataId < this.currentDataId ? -1 : 1
			this.currentDataId = dataId
			const index = this.pages.findIndex(page => page.dataId == dataId);
			let pageInfo = this.pages[index]
			const nowChapters = this.pages.filter(item => item.chapter == pageInfo.chapter && (item.type == 'text' || item.type == 'custom' || item.type == 'slot'))
			let contentIndex = this.contents.findIndex(content => content.chapter == pageInfo.chapter)
			pageInfo.totalPage = nowChapters.length
			pageInfo.currentPage = nowChapters.findIndex(item => item.dataId == pageInfo.dataId) + 1
			if ( this.contents[contentIndex].title ) pageInfo.title = this.contents[contentIndex].title
			this.pageInfo = pageInfo
			this._emitPageInfo(pageInfo, this.pages)
			const nextType = this.pages[index + value] && this.pages[index + value].type
			if ( this.pages[index].type == 'loading' || nextType == 'loading') {
				if (this.moreLoading) return
				this.moreLoading = true;
				const loadChapter = this.pages[index].chapter + value;
				contentIndex = this.contents.findIndex(content => content.chapter == loadChapter)
				if (contentIndex > -1) {
					this.computedPage({
						content: this.contents[contentIndex],
						type: value > 0 ? 'next' : 'prev'
					});
					this.preload(loadChapter)
					this.moreLoading = false;
				} else {
					this.loadmoreFlip(loadChapter, value)
				}
			} else {
				this.startAutoplay()
			}
		}
	}
}