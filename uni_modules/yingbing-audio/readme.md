#使用须知

* 1、这是一个音频播放器，理论上支持所有端，不包含UI
* 2、这个插件基于官方API[uni.createInnerAudioContext()](https://uniapp.dcloud.io/api/media/audio-context.html)和[uni.getBackgroundAudioManager()](https://uniapp.dcloud.net.cn/api/media/background-audio-manager.html)创建
* 3、支持歌词，但全局歌词显示只支持app端和h5端
* 4、有什么不懂可以加群 1087735942

#props属性
| 属性名 | 类型 | 默认值 | 可选值 | 说明 | 兼容性 |
| :---: | :---: | :----: | :----: | :---- | :---: |
| src | String | ---- | ---- | 播放链接 | ---- |
| title | String | ---- | ---- | 原生音频播放器显示标题（全局歌词默认显示文字） | mode=='background' |
| poster | String | ---- | ---- | 原生音频播放器显示背景图 | mode=='background' |
| lyric | Array | ---- | ---- | 歌词集合 | ---- |
| mode | String | inner | inner/background | 组件模式 | background在H5下不支持 |
| playMode | String | ambient | ambient（不中止其他声音播放）/soloAmbient（中止其他声音播放）/playback（中止其他声音，后台播放） | 播放模式 | mode=='inner' |
| loop | Boolean | false | true/false | 循环播放 | ---- |
| duration | Number | ---- | ---- | 自定义播放时长 | ---- |
| autoplay | Boolean | false | true/false | 自动播放 | ---- |
| playbackRate | Number | 1.0 | 0.5/0.8/1.0/1.25/1.5/2.0 | 音频播放倍率 | App 3.4.5+（Android 需要 6 及以上版本）、微信小程序 2.11.0、支付宝小程序、抖音小程序 2.33.0+、快手小程序、百度小程序 3.120.2+ |
| volume | Number | 1 | 0-1 | 音量 | mode=='inner' |
| initialTime | Number | 0 | ---- | 音频播放的开始位置 | ---- |
| lyricConfig | Object | ---- | ---- | 歌词配置 | ---- |
| shareConfig | Object | ---- | ---- | 原生音频播放器分享配置 | mode=='background' |

#event事件
| 事件名 | 参数 | 说明 | 兼容性 |
| :---: | :---: | :---- | :---- |
| canplay | duration | 进入可以播放状态，但不保证后面可以流畅播放，总时长可以从这里取到 | ---- |
| play | ---- | 播放事件 | ---- |
| pause | ---- | 暂停事件 | ---- |
| stop | ---- | 停止事件 | ---- |
| ended | ---- | 自然播放结束事件 | ---- |
| error | ---- | 播放错误事件 | ---- |
| timeUpdate | ---- | 播放进度更新事件 | ---- |
| waiting | ---- | 加载中事件，当数据不足，需要停下来加载时会触发 | ---- |
| seeking | ---- | 进行 seek 操作事件 | mode=='inner' |
| seeked | ---- | 完成 seek 操作事件 | mode=='inner' |
| prev | ---- | 系统音乐播放面板点击上一曲事件 | mode=='background'（iOS only） |
| next | ---- | 系统音乐播放面板点击下一曲事件 | mode=='background'（iOS only） |
| lyricChange | {index: 当前歌词的索引, title: 当前歌词} | 歌词变化事件 | ---- |

#内置方法
| 方法名 | 参数 | 说明 |
| :----- | :---- | :---- |
| toggle | ---- | 播放/暂停 |
| play | ---- | 播放 |
| pause | ---- | 暂停 |
| stop | ---- | 停止 |
| destroy | ---- | 销毁 |
| seek | currentTime | 跳转位置（单位是s） |

#lyricConfig歌词配置
| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :---: | :---: | :----: | :----: | :---- |
| mode | String | 'close' | close（不显示也不计算歌词）/hidden（计算但不显示歌词）/overall（计算并显示歌词） | 歌词模式 |
| fillColor | String | '#2ca2f9' | ---- | 歌词填充颜色 |
| strokeColor | String | '#333333' | ---- | 歌词描边颜色 |
| fontSize | Number | uni.upx2px(35) | ---- | 歌词大小（px） |
| fontFamily | String | 'Times New Roman' | ---- | 歌词字体 |
| fontFace | String | ---- | ---- | 歌词字体路径（仅支持APP） |
| verticalAlign | String | 'bottom' | top/bottom | 歌词上下定位 |
| offset | Number | uni.upx2px(15) | ---- | 歌词上下定位偏移量 |

#shareConfig分享配置（mode=='background'）
| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :---: | :---: | :----: | :----: | :---- |
| shareUrl | String | ---- | ---- | 分享链接 |
| albumName | String | ---- | ---- | 分享专辑名称 |
| singer | String | ---- | ---- | 分享歌手名称 |


#快速开始
```html
	<yingbing-audio src="/static/test.mp3"></yingbing-audio>
```

#自动播放（部分浏览器可能不支持）
```html
	<yingbing-audio autoplay src="/static/test.mp3"></yingbing-audio>
```

#开启背景播放器（H5不支持）
```html
	<yingbing-audio mode="background" src="/static/test.mp3"></yingbing-audio>
```

#监听事件
```html
	<yingbing-audio src="/static/test.mp3" @canplay="handleCanplay" @play="handlePlay" @pause="handlePause"></yingbing-audio>
```
```javascript
export default {
	methods: {
		//加载完毕
		handleCanplay (e) {
			console.log(e.duration)//播放时长
		},
		//播放事件
		handlePlay () {
			console.log('play')
		},
		//暂停事件
		handlePause () {
			console.log('pause')
		}
	}
}
```

#添加歌词
```html
	<yingbing-audio :lyric="lyric" :lyricConfig="lyricConfig" src="/static/test.mp3" @lyricChange="handleLyricChange"></yingbing-audio>
```
```javascript
export default {
	data() {
		return {
			lyric: [{
				time: '0',//歌词显示的时间（单位是s）
				title: '这是第一句歌词'//歌词文本
			},{
				time: '5',
				title: '这是第二句歌词'
			},{
				time: '8',
				title: '这是第三句歌词'
			}],
			lyricConfig: {
				mode: 'overall'
			}
		}
	},
	methods: {
		//歌词改变事件
		handleLyricChange (e) {
			console.log(e)//歌词信息
		}
	}
}
```