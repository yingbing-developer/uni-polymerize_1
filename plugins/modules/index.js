export default {
	chooseFile ({title, suffix, multiple, success, fail}) {
		uni.navigateTo({
			url: `/modules/chooseFile?title=${title || ''}&suffix=${suffix || ''}&multiple=${multiple || ''}`,
			animationType: 'slide-in-right',
			complete: (res) => {
				uni.$once('choose-file', (data) => {
					uni.navigateBack({delta: 1});
					try{
						success ? success(data) : null
					}catch(e){
						fail ? fail(e) : null
					}
				})
			}
		});
	}
}