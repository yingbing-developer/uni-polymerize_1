import { SOURCE, SOURCEBLACKLIST } from '../config.js'

const state = {
	source: uni.getStorageSync(SOURCE) || [], //接口来源
	blacklist: uni.getStorageSync(SOURCEBLACKLIST) || [], //接口黑名单
}

const getters = {
	get (state) {
		return state.source
	},
	getBlacklist (state) {
		return state.blacklist
	}
}

const mutations = {
	set (state, source) {
		state.source = source
		uni.setStorageSync(SOURCE, state.source)
	},
	setBlacklist (state, list) {
		state.blacklist = list
		uni.setStorageSync(SOURCEBLACKLIST, state.blacklist)
	}
}

const actions = {
	add ({state, commit}, source) {
		const sources = [...state.source]
		const index = sources.findIndex(item => item.id == source.id)
		index > -1 ? sources[index] = source : sources.push(source)
		commit('set', sources)
	},
	remove ({state, commit}, id) {
		const sources = [...state.source]
		const index = sources.findIndex(item => item.id == id)
		if ( index > -1 ) sources.splice(index, 1);
		commit('set', sources)
	},
	clear ({commit}, type) {
		commit('set', [])
	},
	switchBlacklist ({state, commit}, id) {
		let list = [...state.blacklist]
		let index = list.indexOf(id)
		if ( index > -1 ) {
			list.splice(index, 1)
		} else {
			list.push(id)
		}
		commit('setBlacklist', list)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}