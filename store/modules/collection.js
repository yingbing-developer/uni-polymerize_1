import { COLLECTION } from '../config.js'

const state = {
	collection: uni.getStorageSync(COLLECTION) || [] //收藏列表
}

const getters = {
	get (state) {
		return state.collection
	}
}

const mutations = {
	set (state, collection) {
		state.collection = collection
		uni.setStorageSync(COLLECTION, state.collection)
	}
}

const actions = {
	add ({state, commit}, params) {
		const list = [...state.collection]
		const index = list.findIndex(item => item.id == params.id)
		index > -1 ? list[index] = params : list.unshift(params)
		commit('set', list)
	},
	remove ({state, commit}, id) {
		const list = [...state.collection]
		const index = list.findIndex(item => item.id == id)
		if ( index > -1 ) list.splice(index, 1);
		commit('set', list)
	},
	clear ({state, commit}, type) {
		const list = state.collection.filter(item => item.type != type)
		commit('set', list)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}