<template>
	<yb-page :style="{'background-color': skinColor.color_bg_1}">
		<yb-nav-bar
		@buttonClick="showActionSheet"
		:options="{
			bgColor: skinColor.color_theme_1,
			color: skinColor.color_4
		}" :buttons="[{
			value: 'inc',
			float: 'right'
		}]" title="数据来源">
			<template #inc>
				<rd-icon name="inc-rect" size="40" :color="skinColor.color_4"></rd-icon>
			</template>
		</yb-nav-bar>
		<yb-tabs custom-class="yb-flex-1" :options="options" :data="tabs" :column="tabs.length < 5 ? tabs.length : 'auto'">
			<template v-for="(tab, i) in tabs" :slot="i">
				<yb-virtual-list
				ref="list"
				custom-class="yb-flex-1"
				:data="tab.list"
				height="100"
				:empty="{
					show: tab.list.length == 0,
					color: skinColor.color_gap_1,
					bgColor: skinColor.color_bg_1
				}">
					<template v-for="(item, index) in tab.list" :slot="index">
						<view class="list padding-gap" @tap="toDetail(i, item)">
							<view class="right">
								<text class="label" :style="{color: skinColor.color_1}">{{item.title}}</text>
							</view>
							<view class="right">
								<yb-switch
								:options="{
									itemColor: skinColor.color_bg_1,
									loading: {
										color: skinColor.color_actived_1
									}
								}" :value="isOpen(item.id)" @change="$store.dispatch('source/switchBlacklist', item.id)" />
								<yb-tap custom-style="margin-left: 20rpx" stop @click="showItemMore(i, item)">
									<rd-icon name="type" size="35" :color="skinColor.color_3"></rd-icon>
								</yb-tap>
							</view>
						</view>
					</template>
				</yb-virtual-list>
			</template>
		</yb-tabs>
		<yb-dialog ref="dialog"></yb-dialog>
		<yb-notify ref="notify"></yb-notify>
		<yb-action-sheet ref="actionSheet"></yb-action-sheet>
		<yb-toast ref="toast"></yb-toast>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	import inSources from '@/assets/api/source.js'
	export default {
		mixins: [appMixin],
		data () {
			return {
				size: 20
			}
		},
		computed: {
			tabs () {
				return [{
					label: '内置来源',
					value: 'in',
					list: inSources.filter(source => !source.isAdult || this.$store.getters['app/getAdult'])
				},{
					label: '外部来源',
					value: 'out',
					list: this.$store.getters['source/get'].filter(source => !source.isAdult || this.$store.getters['app/getAdult'])
				}]
			},
			options () {
				return {
					bgColor: this.skinColor.color_bg_1,
					color: this.skinColor.color_1,
					gapColor: this.skinColor.color_gap_1,
					itemColor: this.skinColor.color_gap_2,
					sliderColor: this.skinColor.color_actived_1,
					focusColor: this.skinColor.color_blue,
					column: this.tabs.length > 5 ? 'auto' : this.tabs.length
				}
			}
		},
		methods: {
			isOpen (id) {
				return this.$store.getters['source/getBlacklist'].indexOf(id) == -1
			},
			toDetail (i, item) {
				if ( i == 0 ) {
					this.$refs.notify.warning('内置来源无法编辑')
				} else {
					this.app.$Router.push({
						path: '/pages/source/form',
						query: {
							form: encodeURIComponent(JSON.stringify(item))
						}
					})
				}
			},
			showActionSheet () {
				this.$refs.actionSheet.show({
					title: {
						text: '更多操作',
						color: this.skinColor.color_2
					},
					itemList: [{
						label: '导入本地来源',
						value: 'local'
					},{
						label: '导入网络来源',
						value: 'net'
					},{
						label: '导出数据来源',
						value: 'drive'
					},{
						label: '添加数据来源',
						value: 'inc'
					},{
						label: '清空数据来源',
						value: 'clear'
					}],
					options: {
						cancel: {
							text: '关闭',
							color: this.skinColor.color_1
						},
						bgColor: this.skinColor.color_bg_2,
						color: this.skinColor.color_1,
						border: {
							color: this.skinColor.color_gap_1,
						}
					},
					success: (res) => {
						if ( res.confirm ) {
							if ( res.data.detail.value == 'local' ) {
								this.chooseFile()
							}
							if ( res.data.detail.value == 'net' ) {
								this.$refs.dialog.prompt({
									async: true,
									title: {
										color: this.skinColor.color_1,
										text: '导入网络来源'
									},
									form: [{
										placeholder: '请输入接口地址（仅支持GET请求）',
										required: true,
										options: {
											color: this.skinColor.color_1
										}
									}],
									options: {
										bgColor: this.skinColor.color_bg_2
									},
									success: res => {
										if ( res.confirm ) {
											this.app.$request.http.get(res.form[0].value, {
												headers: {
													Referer: res.form[0].value
												}
											}).then(res => {
												this.$refs.dialog.hide()
												this.app.$business.leadSource({
													sources: res.data.list.map(item => JSON.parse(item)),
													success: () => {
														this.$refs.notify.success('导入成功')
													},
													fail: err => {
														this.$refs.notify.fail(err.toString())
													}
												})
											}).catch(e => {
												this.$refs.notify.fail(e.errMsg)
												this.$refs.dialog.hide()
											})
										}
									}
								})
							}
							if ( res.data.detail.value == 'drive' ) {
								let sources = inSources.concat(this.$store.getters['source/get'])
								if ( sources.length > 0 ) {
									this.$refs.toast.loading('生成文件中')
									let ress = []
									sources.forEach(async (source, key) => {
										ress.push(await this.app.$business.deriveSource(source))
										if ( key == sources.length - 1 ) {
											this.$refs.toast.hide()
											let message = ''
											ress.forEach(res => {
												message += res.fileName + '：' + (res.root ? res.root : '导出失败') + '\n\n'
											})
											this.$refs.dialog.alert({
												title: {
													text: '导出结果',
													color: this.skinColor.color_1
												},
												content: {
													text: message,
													color: this.skinColor.color_2
												},
												options: {
													bgColor: this.skinColor.color_bg_2
												}
											})
										}
									})
								}
							}
							if ( res.data.detail.value == 'inc' ) {
								this.app.$Router.push({
									path: '/pages/source/form'
								})
							}
							if ( res.data.detail.value == 'clear' ) {
								this.$refs.dialog.confirm({
									title: {
										color: this.skinColor.color_1,
										text: '提示'
									},
									content: {
										color: this.skinColor.color_2,
										text: '清空所有数据来源？\n\n点击确认继续'
									},
									options: {
										bgColor: this.skinColor.color_bg_2
									},
									success: res => {
										if ( res.confirm ) {
											this.$store.dispatch('source/clear')
										}
									}
								})
							}
						}
					}
				})
			},
			remove (id) {
				this.$refs.dialog.confirm({
					title: {
						color: this.skinColor.color_1,
						text: '提示'
					},
					content: {
						color: this.skinColor.color_2,
						text: '删除该数据来源？\n\n点击确认继续'
					},
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							this.$store.dispatch('source/remove', id)
						}
					}
				})
			},
			showItemMore (i, params) {
				let itemList = [{
					label: '导出数据来源',
					value: 'drive'
				}]
				if ( i != 0 ) {
					itemList.push({
						label: '删除数据来源',
						value: 'delete'
					})
				}
				this.$refs.actionSheet.show({
					title: {
						text: params.title,
						color: this.skinColor.color_2
					},
					itemList: itemList,
					options: {
						cancel: {
							text: '关闭',
							color: this.skinColor.color_1
						},
						bgColor: this.skinColor.color_bg_2,
						color: this.skinColor.color_1,
						border: {
							color: this.skinColor.color_gap_1,
						}
					},
					success: (res) => {
						if ( res.confirm ) {
							if ( res.data.detail.value == 'delete' ) {
								this.remove(params.id)
							}
							if ( res.data.detail.value == 'drive' ) {
								this.app.$business.deriveSource(params).then(res => {
									if ( res.status ) {
										this.$refs.dialog.confirm({
											title: {
												text: '文件路径',
												color: this.skinColor.color_1
											},
											content: {
												text: res.root + res.fileName,
												color: this.skinColor.color_2
											},
											cancel: '关闭窗口',
											confirm: '我要分享',
											options: {
												bgColor: this.skinColor.color_bg_2
											},
											success: ret => {
												if ( ret.confirm ) {
													this.app.$business.shareFile(res.root + res.fileName)
												}
											}
										})
									} else {
										this.$refs.notify.error(res.msg)
									}
								})
							}
						}
					}
				})
			},
			
			//导入JSON
			chooseFile () {
				getApp().globalData.$modules.chooseFile({
					title: '选择来源',
					suffix: 'json',
					multiple: true,
					success: (res) => {
						if ( res.confirm ) {
							let sources = []
							this.$refs.toast.loading('读取文件中')
							for ( let i = 0; i < res.data.files.length; i++ ) {
								try{
									let ff = res.data.files[i]
									let fs = plus.android.newObject("java.io.FileInputStream", ff.path);
									let isr = plus.android.newObject("java.io.InputStreamReader", fs, 'UTF-8');
									let br = plus.android.newObject("java.io.BufferedReader", isr);
									let sb = ''
									let temp = ''
									while ((temp = plus.android.invoke(br, 'readLine')) != null) {
									    sb += temp;
									}
									plus.android.invoke(br, 'close')
									sources.push(JSON.parse(sb))
								}catch(e){
									//TODO handle the exception
									continue
								}
							}
							if ( sources.length > 0 ) {
								this.app.$business.leadSource({
									sources: sources,
									success: res => {
										this.$refs.toast.hide()
										this.$refs.notify.success('导入成功')
									},
									fail: err => {
										this.$refs.toast.hide()
										this.$refs.notify.fail(err.toString() || '导入失败')
									}
								})
							} else {
								this.$refs.toast.hide()
								this.$refs.notify.fail('来源读取失败')
							}
							// res.data.files.forEach(ff => {
								// plus.io.resolveLocalFileSystemURL(ff.path, ( entry ) => {
								// 	// 可通过entry对象操作test.html文件 
								// 	entry.file( (file) => {
								// 		let fileReader = new plus.io.FileReader();
								// 		fileReader.readAsText(file, 'UTF-8');
								// 		fileReader.onloadend = (evt) => {
								// 			try{
								// 				let data = evt.target.result.replace(/\r|\n|\t/g, '')
								// 				let requesValues = data.match(/\"value\": \"*([\s\S]*?)\"}/ig)
								// 				if ( requesValues ) {
								// 					requesValues.forEach((value, key) => {
								// 						let val = value.match(/\"value\": \"*([\s\S]*?)\"}/)[1]
								// 						data = data.replace(val, key)
								// 					})
								// 				}
								// 				let result = JSON.parse(data)
								// 				Object.keys(result.request).forEach(key => {
								// 					if ( result.request[key].value ) {
								// 						let val = requesValues[result.request[key].value].match(/\"value\": \"*([\s\S]*?)\"}/)[1]
								// 						result.request[key].value = val
								// 					}
								// 				})
								// 				this.$store.dispatch('source/add', new Source({
								// 					id: result.id,
								// 					title: result.title,
								// 					href: result.href,
								// 					isAdult: result.isAdult,
								// 					isOpen: true,
								// 					request: result.request,
								// 					type: result.type
								// 				}))
								// 			}catch(e){
								// 				throw new Error('错误的文件来源');
								// 			}
								// 		}
								// 	} );
								// }, ( err ) => {
								// 	throw new Error('错误的文件来源');
								// });
							// })
						}
					}
				})
			}
		}
	}
</script>

<style scoped>
	.list {
		flex-direction: row;
		align-items: center;
		justify-content: space-between;
		height: 100rpx;
	}
	.list .label {
		font-size: 28rpx;
		flex: 1;
		text-overflow: ellipsis;
		lines: 1;
		margin-right: 10rpx;
	}
	.list .right {
		flex-direction: row;
		align-items: center;
	}
</style>
