<template>
	<yb-page :style="{'background-color': skinColor.color_bg_1}">
		<yb-nav-bar
		@inputClick="app.$Router.push({
			path: '/pages/search/index'
		})"
		@buttonClick="onButtonClick"
		tabbar
		:input="{
			show: true,
			color: skinColor.color_2,
			bgColor: skinColor.color_gap_1
		}"
		:buttons="[{
			value: 'paging',
			right: 'right'
		},{
			value: 'filter',
			right: 'right'
		},{
			value: 'followee',
			right: 'right'
		}]"
		transparent
		:options="{
			color: skinColor.color_1,
			statusBarColor: 'rgba(0,0,0,.4)'
		}">
			<template #followee>
				<rd-icon name="avatar" :size="42" :color="skinColor.color_1" />
			</template>
			<template #paging>
				<rd-icon name="paging" :size="37" weight="bold" :color="skinColor.color_1" />
			</template>
			<template #filter>
				<rd-icon name="filter-circle" :size="45" :color="skinColor.color_1" />
			</template>
		</yb-nav-bar>
		<yb-grid custom-class="padding-gap" :data="sorts" :column="sorts.length">
			<template v-for="(grid, i) in sorts" :slot="i">
				<yb-tap @click="changeSort(grid.value)">
					<yb-tag
					:options="{
						bgColor: skinColor.color_gap_1,
						color: sort == grid.value ? skinColor.color_actived_1 : skinColor.color_3
					}"
					:value="grid.label"></yb-tag>
				</yb-tap>
			</template>
		</yb-grid>
		<yb-gap custom-class="dynamic_gap" :custom-style="{
			'border-bottom-color': skinColor.color_gap_1
		}" size="20"></yb-gap>
		<view class="yb-flex yb-flex-1" style="position:relative;">
			<rd-cells
			:list="list"
			ref="list"
			pulldown
			:empty="list.length == 0 && isLastPage"
			:loading="loading"
			:loadmore="{
				show: true,
				bottom: '30px'
			}"
			@loadmore="onLoadmore"
			@pulldown="onPulldown"></rd-cells>
			<rd-filter :value="type" :visible.sync="filterShow" @confirm="changeType"></rd-filter>
		</view>
		<yb-dialog ref="dialog"></yb-dialog>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js';
	export default {
		mixins: [appMixin],
		data () {
			return {
				list: [],
				loading: true,
				isLastPage: false,
				pagings: {},
				isLastPages: {},
				requestTime: 0,
				currentPage: 1,
				sorts: [{
					label: '按时间',
					value: 'newest'
				},{
					label: '按热度',
					value: 'hottest'
				},{
					label: '按关注',
					value: 'followee'
				},{
					label: '按收藏',
					value: 'collection'
				},{
					label: '按喜好',
					value: 'record'
				}],
				sort: 'newest',
				type: 'compress',
				filterShow: false
			}
		},
		onShow() {
			this.app.$business.setSkinColor()
		},
		onReady() {
			this.$nextTick(function () {
				setTimeout(() => {
					this.onPulldown()
				}, 500)
			})
		},
		methods: {
			onButtonClick (e) {
				if ( e.value == 'filter' ) {
					this.filterShow = !this.filterShow
				} else if ( e.value == 'paging' ) {
					this.jumpPage()
				} else {
					this.app.$Router.push({
						path: '/pages/my/followee'
					})
				}
			},
			changeSort (value) {
				this.filterShow = false
				this.sort = value
				this.loading = true
				this.$nextTick(function () {
					this.list = []
					this.onPulldown()
				})
			},
			changeType (value) {
				if ( this.type != value ) {
					this.filterShow = false
					this.type = value
					this.loading = true
					this.$nextTick(function () {
						this.list = []
						this.onPulldown()
					})
				}
			},
			async getList () {
				if ( this.sort == 'newest' || this.sort == 'hottest' ) {
					return await this.app.$business.requestApis({
						name: 'getDynamicList',
						type: this.type,
						data: {
							currentPage: this.currentPage,
							sort: this.sort
						},
						size: 1,
						pagings: this.pagings,
						isLastPages: this.isLastPages,
						requestTime: this.requestTime
					}).then(data => {
						return data
					})
				} else if ( this.sort == 'followee' ) {
					return await this.app.$business.getFolloweeList({
						type: this.type,
						size: 1,
						pagings: this.pagings,
						currentPage: this.currentPage,
						isLastPages: this.isLastPages,
						requestTime: this.requestTime
					}).then(data => {
						return data
					})
				} else {
					return await this.app.$business.getRecome({
						type: this.type,
						size: 1,
						pagings: this.pagings,
						currentPage: this.currentPage,
						isLastPages: this.isLastPages,
						requestTime: this.requestTime,
						by: this.sort
					}).then(data => {
						return data
					})
				}
			},
			jumpPage () {
				this.$refs.dialog.prompt({
					title: {
						color: this.skinColor.color_1,
						text: '跳转页码（仅支持以页码跳转的源）'
					},
					value: this.currentPage,
					type: 'number',
					test: '^([1-9][0-9]*){1,3}$',
					required: true,
					error: '请输入非零的正整数',
					placeholder: '请输入页数',
					options: {
						bgColor: this.skinColor.color_bg_2,
						color: this.skinColor.color_1
					},
					success: ret => {
						if ( ret.confirm ) {
							this.loading = true
							this.$refs.list.resetLoadmore()
							this.currentPage = parseInt(ret.form[0].value)
							this.pagings = {}
							this.isLastPages = {}
							this.requestTime = 0
							this.isLastPage = false
							this.getList().then(data => {
								this.list = data.list
								if ( data.list.length > 0 ) {
									data.isLastPage ? this.$refs.list.setLoadmoreEnd() : null
								} else {
									data.isLastPage ? this.$refs.list.setLoadmoreEnd() : this.$refs.list.setLoadmoreFail()
								}
								this.isLastPage = data.isLastPage
								this.pagings = data.pagings || {}
								this.isLastPages = data.isLastPages || {}
								this.currentPage = data.currentPage
								this.requestTime = data.requestTime
								this.loading = false
							})
						}
					}
				})
			},
			onPulldown (callback) {
				this.$refs.list.resetLoadmore()
				this.currentPage = 1
				this.pagings = {}
				this.isLastPages = {}
				this.requestTime = 0
				this.isLastPage = false
				this.getList().then(data => {
					callback && callback('success')
					this.list = data.list
					if ( data.list.length > 0 ) {
						data.isLastPage ? this.$refs.list.setLoadmoreEnd() : null
					} else {
						data.isLastPage ? this.$refs.list.setLoadmoreEnd() : this.$refs.list.setLoadmoreFail()
					}
					this.isLastPage = data.isLastPage
					this.pagings = data.pagings || {}
					this.isLastPages = data.isLastPages || {}
					this.currentPage = data.currentPage
					this.requestTime = data.requestTime
					this.loading = false
				})
			},
			onLoadmore (callback) {
				this.getList().then(data => {
					this.list = this.list.concat(data.list)
					this.isLastPage = data.isLastPage
					if ( data.list.length > 0 ) {
						callback(data.isLastPage ? 'end' : 'success')
					} else {
						callback(data.isLastPage ? 'end' : 'fail')
					}
					this.pagings = data.pagings || {}
					this.isLastPages = data.isLastPages || {}
					this.currentPage = data.currentPage
					this.requestTime = data.requestTime
				})
			}
		}
	}
</script>

<style>
	.dynamic_gap {
		border-bottom-width: 1px;
		border-bottom-style: solid;
	}
</style>
