<template>
	<yb-page :style="{'background-color': skinColor.color_bg_1}">
		<yb-nav-bar :options="{
			bgColor: skinColor.color_theme_1,
			color: skinColor.color_4
		}" title="浏览器设置"></yb-nav-bar>
		<yb-list custom-class="yb-flex-1" type="scroll">
			<view class="list padding-gap" :style="{'border-color': skinColor.color_gap_1}">
				<text class="label" :style="{color: skinColor.color_1}">浏览记录</text>
				<yb-switch
				:options="{
					itemColor: skinColor.color_bg_1,
					loading: {
						color: skinColor.color_actived_1
					}
				}"
				ref="switch" :value="$store.getters['app/getFootmark']" @change="$store.dispatch('app/switchFootmark')" />
			</view>
			<view class="list padding-gap" :style="{'border-color': skinColor.color_gap_1}">
				<text class="label" :style="{color: skinColor.color_1}">无图模式</text>
				<yb-switch
				:options="{
					itemColor: skinColor.color_bg_1,
					loading: {
						color: skinColor.color_actived_1
					}
				}"
				ref="switch" :value="$store.getters['browser/getBlockNetworkImage']" @change="$store.dispatch('browser/switchBlockNetworkImage')" />
			</view>
			<view class="list padding-gap" :style="{'border-color': skinColor.color_gap_1}" @tap="handleStartPage">
				<text class="label" :style="{color: skinColor.color_1}">启动页</text>
				<rd-icon name="arrow-right" size="40" :color="skinColor.color_1"></rd-icon>
			</view>
			<view class="list padding-gap" :style="{'border-color': skinColor.color_gap_1}" @tap="handleShield">
				<text class="label" :style="{color: skinColor.color_1}">屏蔽规则</text>
				<rd-icon name="arrow-right" size="40" :color="skinColor.color_1"></rd-icon>
			</view>
		</yb-list>
		<yb-dialog ref="dialog"></yb-dialog>
		<yb-notify ref="notify"></yb-notify>
		<yb-editor ref="editor"></yb-editor>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js';
	export default {
		mixins: [appMixin],
		methods: {
			handleStartPage () {
				this.$refs.dialog.prompt({
					title: {
						color: this.skinColor.color_1,
						text: '修改启动页'
					},
					form: [{
						placeholder: '启动页链接',
						required: true,
						value: this.$store.getters['browser/getStartpage'],
						options: {
							color: this.skinColor.color_1
						}
					}],
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							this.$store.commit('browser/setStartpage', res.form[0].value)
							this.$refs.notify.success('启动页修改为：' + res.form[0].value)
						}
					}
				})
			},
			handleShield () {
				this.$refs.editor.show({
					title: '编辑屏蔽规则',
					placeholder: '请输入正则表达式，如：.*qq\\.com\/.*',
					value: this.$store.getters['browser/getShield'],
					cancel: {
						color: this.skinColor.color_1
					},
					options: {
						bgColor: this.skinColor.color_bg_2,
						color: this.skinColor.color_1,
						textareaColor: this.skinColor.color_gap_1
					},
					success: res => {
						if ( res.confirm ) {
							this.$store.commit('browser/setShield', res.data.value || '')
							this.$refs.notify.success('编辑屏蔽规则成功')
						}
					}
				})
			}
		}
	}
</script>

<style scoped>
	.list {
		flex-direction: row;
		align-items: center;
		justify-content: space-between;
		height: 100rpx;
		border-bottom-width: 1rpx;
	}
	.list .label {
		font-size: 28rpx;
	}
</style>
