<template>
	<view class="music">
		<yb-popup :visible="show" @show="onPopupShow" @hide="onhide">
			<view class="popup" :style="{'background-color': skinColor.color_bg_1, height: height + 'px' }">
				<view class="top yb-flex yb-row yb-align-center yb-justify-between" :style="{'border-color': skinColor.color_gap_1}">
					<text class="title" :style="{color: skinColor.color_2}">播放列表（{{playList.length}})</text>
					<view class="top-right">
						<view class="tr-icon" @tap="show = false">
							<yb-icon name="fork" size="34" :color="skinColor.color_1"></yb-icon>
						</view>
					</view>
				</view>
				<yb-virtual-list
				ref="list"
				:data="playList"
				height="80"
				:loading="{
					show: loading,
					bgColor: skinColor.color_bg_2,
					color: skinColor.color_1
				}"
				:empty="{
					text: '播放列表为空',
					show: playList.length == 0,
					bgColor: skinColor.color_bg_2,
					color: skinColor.color_gap_1
				}"
				:loadmore="{
					show: currentAlbum ? true : false,
					color: skinColor.color_1
				}"
				@loadmore="onLoadmore">
					<template v-for="(item, index) in playList" :slot="index">
						<view class="list" @tap="play(item)">
							<view>
								<rd-icon v-if="currentId == item.id" name="music" :color="skinColor.color_red"></rd-icon>
								<rd-icon v-else name="music" :color="skinColor.color_1"></rd-icon>
							</view>
							<text class="list-name" :style="{color: currentId == item.id ? skinColor.color_red : skinColor.color_1}">{{item.title}}</text>
							<view class="list-right">
								<yb-text :color="skinColor.color_red" size="26" value="付费" v-if="item.isPay"></yb-text>
								<yb-tap custom-class="list-icon" stop @click="remove(item)">
									<rd-icon name="dustbin-fill" size="40" :color="skinColor.color_1"></rd-icon>
								</yb-tap>
							</view>	
						</view>
					</template>
				</yb-virtual-list>
				<view class="clear" v-if="playList.length > 0">
					<yb-button @click="clear" custom-class="clear-btn" :options="{
						size: 24
					}" type="primary" value="清除全部歌曲"></yb-button>
				</view>
			</view>
		</yb-popup>
		<yb-dialog ref="dialog"></yb-dialog>
	</view>
</template>

<script>
	import appMixin from '@/common/mixin/app.js';
	const animation = weex.requireModule('animation');
	export default {
		mixins: [appMixin],
		data () {
			return {
				show: false,
				isShowList: false,
				loading: true,
				windowHeight: 0
			}
		},
		computed: {
			height () {
				return this.windowHeight / 1.5
			},
			currentAlbum () {
				return this.$store.getters['audio/getCurrentAlbum']
			},
			currentPage () {
				return this.$store.getters['audio/getCurrentPage']
			},
			currentPaging () {
				return this.$store.getters['audio/getCurrentPaging']
			},
			currentIsLastPage () {
				return this.$store.getters['audio/getCurrentIsLastPage']
			},
			song () {
				return this.$store.getters['audio/getCurrentSong']
			},
			currentId () {
				return this.song ? this.song.id : ''
			},
			playList () {
				return this.isShowList ? this.$store.getters['audio/getPlayList'] : []
			}
		},
		onReady() {
			this.windowHeight = uni.getSystemInfoSync().windowHeight
			this.show = true
			this.$nextTick(function () {
				setTimeout(() => {
					if ( this.currentIsLastPage || !this.currentAlbum ) {
						this.$refs.list && this.$refs.list.setLoadmoreEnd()
					}
				}, 200)
			})
		},
		methods: {
			onPopupShow () {
				this.isShowList = true
				this.$nextTick(function () {
					setTimeout(() => {
						let index = this.playList.findIndex(audio => audio.id == this.currentId)
						if ( index > -1 ) this.$refs.list.scrollToIndex(index)
						this.loading = false
					}, 500)
				})
			},
			onhide () {
				getApp().globalData.$Router.back();
			},
			play (song) {
				if ( song.id != this.currentId ) this.$store.commit('audio/setCurrentSong', song)
			},
			remove (item) {
				this.$store.dispatch('audio/removeSong', item.id)
				this.$nextTick(function () {
					this.$store.dispatch('audio/next')
				})
			},
			clear () {
				this.$refs.dialog.confirm({
					title: {
						color: this.skinColor.color_1,
						text: '提示'
					},
					content: {
						color: this.skinColor.color_2,
						text: '清空播放列表？\n\n点击确认继续'
					},
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							this.$store.dispatch('audio/destroy')
						}
					}
				})
			},
			async getList () {
				return await this.app.$api.getGatherWorksList( Object.assign({}, this.currentAlbum, {
					currentPage: this.currentPage,
					paging: this.currentPaging
				})).then(res => {
					return res
				})
			},
			onLoadmore (callback) {
				this.$store.commit('audio/setCurrentPage', this.currentPage + 1)
				this.getList().then(res => {
					if ( res.code == this.app.$config.ERR_OK ) {
						callback(res.data.isLastPage ? 'end' : 'success')
						this.$store.commit('audio/setCurrentPaging', res.data.paging || {})
						this.$store.dispatch('audio/addSong', res.data.list)
						this.$store.commit('audio/setCurrentIsLastPage', res.data.isLastPage)
					} else {
						callback('fail')
					}
				})
			}
		},
		onBackPress (event) {
			if ( event.from == 'backbutton' ) {
				this.show = false
				return true;
			}
			return false
		}
	}
</script>

<style scoped>
	.popup {
		border-top-left-radius: 20rpx;
		border-top-right-radius: 20rpx;
	}
	.top {
		padding: 20rpx 30rpx;
		border-bottom-width: 1px;
	}
	.top-right {
		flex-direction: row;
		align-items: center;
	}
	.tr-icon {
		margin-left: 20rpx;
	}
	.title {
		font-size: 28rpx;
	}
	.list {
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
		height: 80rpx;
		padding: 20rpx 30rpx;
	}
	.list-right {
		flex-direction: row;
		align-items: center;
	}
	.list-icon {
		margin-left: 20rpx;
	}
	.list-name {
		flex: 1;
		font-size: 28rpx;
		margin-left: 10rpx;
		lines: 1;
		text-overflow: ellipsis;
	}
	.clear {
		justify-content: center;
		align-items: center;
		padding: 20rpx 0;
	}
	.clear-btn {
		width: 220rpx;
		height: 60rpx;
	}
	.close-text {
		font-size: 25rpx;
	}
	.scroll {
		flex: 1;
	}
</style>
